'use strict';

console.log(__dirname);
let Init = require('./lib/init');
/* ------------------------------ Loading context --------------------------------- */

new Init().register([
  require('./routes/index'),
  require('./services/user'),
  require('./services/db')
]).then((ctx) => {
  ctx.getBean('rest').prepare();
  console.log("Application initialized");
}).catch((e) => {
  console.log(e.stack || e)
});


/* -------------------------- Catch unexpected exceptions -------------------------- */

process.on('uncaughtException', function (err) {
  console.log('Caught exception: ', err.stack || err.toString());
});

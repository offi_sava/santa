'use strict';

var _ = require('lodash');


/* format ok response object */

var okResponse = function(obj, paging) {
    return {
        'meta': {
            'status': 'ok',
            'error': null,
            'paging': paging
        },
        'response': obj || null
    };
};


/* format error response object, */
/* depending on kind of error    */

var errResponse = function(err) {

    return {
        'meta': {
            'status': 'error',
            'error': {
                'message': err
            }
        },
        'response': null
    };
};



/* exports */

module.exports = {

    errResponse: errResponse,

    okResponse: okResponse,

    makeResponse: function(err, obj, pagination) {
        return err ? errResponse(err) : okResponse(obj, pagination);
    }
};


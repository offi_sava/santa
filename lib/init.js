'use strict';

const _ = require('lodash'),
    promisify = require('util').promisify;

module.exports = class Injector {

    constructor() {
        this.deps = {};
        this.context = {};
    }

    async register(dependencies) {

        if (!_.isArray(dependencies)) {
            throw Error('Registering dependencies must be in format [Class1, Class2, ...]');
        }

        for (const Dep of dependencies) {

            let dep = new Dep();

            if (!_.isFunction(dep.initialize)) {
                throw Error(`Dependency ${Dep.name} has no initialize method`);
            }

            this.context[_.lowerFirst(Dep.name)] = {
                $$obj: dep,
                deps:  _.isFunction(Dep.dependencies) ? Dep.dependencies() : []
            };

            await promisify((...args) => {dep.initialize(...args)})();
        }

        _.each(this.context, (d, name) => {
            _.each(d.deps, dd => {

                if (dd === 'context') {
                    d.$$obj[dd] = this.context
                }

                if (!_.isEmpty(this.context[dd])) {
                    d.$$obj[dd] = !_.has(this.context[dd], '$$obj') ? this.context[dd] : this.context[dd].$$obj
                }
            });
            this.context[name] = d.$$obj
        });

        this.context.getBean = (name) => {
            if (_.has(this.context, name)) {
                return this.context[name];
            } else {
                return {};
            }
        };

        _.each(this.context, (d) => {
            if (_.isFunction(d.ready)) {
                d.ready();
            }
        });

        return this.context;
    }
}

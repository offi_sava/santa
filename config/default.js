
module.exports = {

    'rest': {
        'secure': true,
        'port': 3000,
        'keepAliveTimeout': 60 * 1000,
        'enabled': true
    },
    'database' : {
        dialect: 'mysql', // use `mysql` for mysql
        username: 'root',
        password: '',
        database: 'santa',
        host: 'localhost',
        dialectOptions: {
            ssl: false
        }
    }

}
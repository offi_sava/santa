ALTER TABLE users rename COLUMN id to telegram_id;
ALTER TABLE users rename COLUMN login_telegram to telegram_login;

alter table users_rooms
    drop foreign key users_rooms_user_id_foreign;
alter table users_santas
    drop foreign key users_santas_user_id_foreign;
alter table users_santas
    drop foreign key users_santas_santa_for_user_id_foreign;
alter table orders
    drop foreign key orders_user_id_foreign;

alter table users
    drop primary key;

alter table users
    modify column telegram_id bigint unsigned default null first;

alter table users
    add column `id` bigint unsigned primary KEY AUTO_INCREMENT first;


SET FOREIGN_KEY_CHECKS = 0;

alter table users_rooms
    add constraint users_rooms_user_id_foreign
        foreign key (user_id) references users (id)
            on delete cascade;

alter table users_santas
    add constraint users_santas_santa_for_user_id_foreign
        foreign key (santa_for_user_id) references users (id)
            on delete cascade;
alter table users_santas
    add constraint users_santas_user_id_foreign
        foreign key (user_id) references users (id)
            on delete cascade;

alter table orders
    add constraint orders_user_id_foreign
        foreign key (user_id) references users (id)
            on delete cascade;

SET FOREIGN_KEY_CHECKS = 1;


update users_rooms as u_r
set user_id = (select u.id from users as u where u.telegram_id = u_r.user_id);

update orders as o
set o.user_id = (select u.id from users as u where u.telegram_id = o.user_id);

update users_santas as u_s
set u_s.santa_for_user_id = (select u.id from users as u where u.telegram_id = u_s.santa_for_user_id );
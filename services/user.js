'use strict';

const _ = require('lodash'),
    random = require('random-bigint');


module.exports = class User {

    static dependencies() {
        return ['db']
    }

    /**
     * Initialize
     */
    initialize(callback) {
        console.log('Initializing services/user...');
        callback();
    }

    async getUserById(userId) {
         return this.db.User.findOne({
            where: {
                id: userId
            },
            include: [
                {
                    model: this.db.UserSanta,
                    required: false,
                    as: 'userSanta',
                },
                {
                    model: this.db.UserRoom,
                    required: false,
                    as: 'room',
                }
            ]
        })
    }


    async getAllUsers(offset, limit) {

        return this.db.User.findAll({
            limit: limit,
            offset: offset
        })
    }

    async createUser(options) {
       return this.db.User.create(options)
    }

    async updateUserById(userId, options) {
        let user = await this.db.User.findOne({ where: { id: userId }});

        if (_.isEmpty(user)) {
            throw new Error('User with this id no found')
        }
        console.log(user);
        console.log(userId);
        await user.update(options);
    }
}
'use strict';

let _ = require('lodash'),
    util = require('util'),
    fs = require('fs'),
    path = require('path'),
    cls = require('cls-hooked'),
    namespace = cls.createNamespace('rest-santa-transaction-namespace'),
    sequelize = require('sequelize'),
    dbConfig = require("../config/db");

let modelPath = path.resolve(__dirname + '/../models/');

sequelize.useCLS(namespace);

let orm = new sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    operatorsAliases: false,

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});



module.exports = class Db {

    constructor() {
        this.orm = orm;
        this.sequelize = sequelize;
    }

    /**
     * Initialize
     */
    initialize(callback) {

        console.log("Initializing database...");


        /* loading ORM models */
        _.each(fs.readdirSync(modelPath), (file) => {
            // skip dirs
            if (!fs.statSync(modelPath + '/' + file).isFile()) {
                return;
            }

            this[_.upperFirst(_.camelCase(file.split('.')[0]))] = orm["import"](path.join(modelPath, file));
        });

        /* create associations between models */
        _.each(this, (model) =>{
            if (_.has(model, 'associate')){
                model.associate(this);
            }
        });

        callback();
    }

};



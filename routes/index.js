'use strict'

const createError = require('http-errors'),
    express = require('express'),
    path = require('path'),
    config = require('../config/default'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    logger = require('morgan'),
    r = require('../lib/api-response'),
    http = require('http'),
    https = require('https');


/**
 * @Bean('rest')
 */
module.exports = class Rest {

  static dependencies() {
    return ['context']
  }

  /**
   * @Initialize
   */
  initialize(callback) {

    /* Server init */
    console.log("Inializing server...");

    this.app = express();

    var server = http.createServer(this.app);

    server.listen(config.rest.port, () => {
      console.log("Server listening on port", server.address().port);
    });

    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: false }));
    this.app.use(cookieParser());
   //this. app.use(express.static(path.join(__dirname, 'public')));

    this.app.use(bodyParser.json({strict:false})); // turn off strict to accept "strings" as body
    this.app.set('json spaces', 4);

    callback();
  }

  prepare() {

    this.app.all('*', require('./filters/allowcrossdomain'));

    this.app.use('/', require('./main')(this.context));
    this.app.use('/users', require('./main/users')(this.context));

// catch 404 and forward to error handler
    this.app.use(function(req, res, next) {
      next(createError(404));
    });

// error handler
    this.app.use(function(err, req, res, next) {
      // set locals, only providing error in development
      res.locals.message = err.message;
      res.locals.error = req.app.get('env') === 'development' ? err : {};

      // render the error page
      res.status(err.status || 500);
      res.render('error');
    });
  }

}

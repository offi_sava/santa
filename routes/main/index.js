const express = require('express'),
    router = express.Router(),
    r = require('../../lib/api-response');


module.exports = function() {

    router.get('/', async (req, res) => {
        try {
            let users = await db.Call.findAll();
           console.log(users);
            res.json(r.okResponse(users));
        } catch (err) {
            res.json(r.errResponse(err));
        }
    });

    return router;
};

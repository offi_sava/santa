const _ = require('lodash'),
    express = require('express'),
    router = express.Router(),
    r = require('../../lib/api-response');

module.exports = function(context) {

  const db = context.getBean('db'),
      user = context.getBean('user');

  const paramsRequired = require('../filters/params'),
      schemaRequired = require('../filters/validation').schemaRequired;

  router.post('/', schemaRequired({
    'additionalProperties': false,
    'required': ['fullname'],
    'properties': {
      'telegramLogin': {
        'type': 'string'
      },
      'telegramId': {
        'type': 'string'
      },
      'country': {
        'type': 'string'
      },
      'city': {
        'type': 'string'
      },
      'phone': {
        'type': 'string'
      },
      'address': {
        'type': 'string'
      },
      'fullname': {
        'type': 'string'
      }
    }
  }), async (req, res) => {
    try {
      //if (!req.body.fullname) return res.json(r.errResponse('Need fullname in body'));
      let newUser = await user.createUser(req.body);
      res.json(r.okResponse(_.pick(newUser, 'id')));
    } catch (err) {
      console.log(err);
      res.json(r.errResponse(err.message));
    }
  });


  router.get('/',  async (req, res) => {
    try {
      let offset = req.query.offset ? req.query.offset : 50;
      let users = await user.getAllUsers(
          parseInt(req.query.offset) || 50,
          parseInt(req.query.limit)|| 50
      );
      res.json(r.okResponse(users));
    } catch (err) {
      console.log(err);
      res.json(r.errResponse(err.message));
    }
  });


  router.get('/:id', async (req, res) => {
    try {
      let result = await user.getUserById(req.params.id)
      res.json(r.okResponse(result));
    } catch (err) {
      console.log(err);
      res.json(r.errResponse(err.message));
    }
  });


  router.put('/:id', schemaRequired({
    'additionalProperties': false,
    'properties': {
      'country': {
        'type': 'string'
      },
      'city': {
        'type': 'string'
      },
      'phone': {
        'type': 'string'
      },
      'address': {
        'type': 'string'
      },
      'fullname': {
        'type': 'string'
      },
      'wish': {
        'type': 'string'
      }
    }
  }), async (req, res) => {
    try {
      let result = await user.updateUserById(req.params.id, req.body)
      res.json(r.okResponse(result));
    } catch (err) {
      console.log(err);
      res.json(r.errResponse(err.message));
    }
  });




  return router;
};

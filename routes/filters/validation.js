'use strict';

var _ = require('lodash'),
    ajv = require('ajv'),
    r = require('../../lib/api-response');

    const rules = {
        email: {
            regex: '^(?!.*\\_{2})(?!.*\\-{2})(?!.*\\.{2})(?!.*\\%{2})(?!\\.)(?!.*\\])(?!.*\\[)[A-z0-9.+_%-]+@[A-z0-9.-]+\\.[A-z]{2,18}$',
            message: 'Please enter valid email'
        },
        password: {
            regex: '^(?=.*\\d+)(?=.*[A-Z]+)(?=.*[a-z]+)(?=.*[~!@#$%^&*_\\-+=`|\\\\(){}\\[\\]:;"\'<>,.?\\/]+)[A-z~!@#$%^&*_\\-+=`|\\(){}\\[\\]:;"\'<>,.?\\/\\d]{8,64}$',
            message: 'Password must contain between 8 and 64 characters, at least one upper case letter, one lower case letter, one non-alphanumeric characters and one numeric digit'
        },
        firstName: {
            regex: '^.{2,20}$',
            message: 'First name must contain min 2 max 20 characters'
        },
        lastName: {
            regex: '^.{2,20}$',
            message: 'Last name must contain min 2 max 20 characters'
        }

    };

/* exports */
module.exports = {

    /**
     * Filter validates request body to fit json schema
     */
    schemaRequired: function(schema) {
        var v = new ajv({
            //schemaId: 'id',
            allErrors: true,
            removeAdditional: false,
            verbose: true,
            jsonPointers: true
        });

        require('ajv-errors')(v);

        _.each(rules, (rule, name) => {
            v.addFormat(name, rule.regex)
        });

        return function(req, res, next) {

            let validate = v.compile(schema);
            let valid = validate(req.body);

            if (valid) return next();

            let errs = validate.errors;

            let formattedErrors = _.map(errs, function(err) {

                let type = err.keyword || 'validation_error';

                switch (err.keyword) {
                    case 'format':
                        return {
                            type: type,
                            message: rules[err.params.format].message
                        };
                    case 'errorMessage':
                        return {
                            type: type,
                            message: err.message
                        };
                    default:
                        return {
                            type: type,
                            message: err.dataPath ? `Field ${err.dataPath} ${err.message}` : `Body ${err.message}`
                        }
                }

            });
            res.json(r.errResponse('malformed_request', 'Bad body format', formattedErrors));
        };
    }

};


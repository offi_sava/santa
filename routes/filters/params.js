'use strict';

var _ = require('lodash'),
    r = require('../../lib/api-response');

/**
 * Simple parameters validation.
 */
module.exports = function(rules) {

    return function(req, res, next) {

        var fail = false;

        // Go through rules and validate query params
        _.each(rules, function(rule, param) {

            if (fail) return;

            var value = req.query[param],
                exists = (value !== undefined && value !== null);

            if (exists && !_.isString(value)) {
                fail = true;
                return res.json(
                    r.errResponse(
                        new ApiError('malformed_request', 'Duplicate parameter: ' + param)
                    )
                );
            }


            if (rule.required && !exists) {
                fail = true;
            } else if (exists && !value.match(rule.pattern)) {
                fail = true;
            } else if (!rule.required && !exists && rule.default) {
                req.query[param] = rule.default;
            }
            if (!fail && rule.converter && !_.isUndefined(req.query[param])) {
                req.query[param] = rule.converter(req.query[param]);
            }

            if (fail) {
                return res.json(
                    r.errResponse('malformed_request', 'No or invalid query parameter: ' + param)
                );
            }
        });

        if (!fail) return next();
    };

};


'use strict';

process.env['SERVER_CONFIG'] = __dirname + '/../locals/config.json';

const dbdiff = require('dbdiff'),
    config = require('../config/default'),
    moment = require('moment'),
    fs = require('fs');

let currentSchema = fs.readFileSync(__dirname + '/../dbdata/currentState.json');

currentSchema = JSON.parse(currentSchema);

dbdiff.describeDatabase(config.database).then((newSchema) => {

    let dd = new dbdiff.DbDiff();

    let result = {
        UP: null,
        DOWN: null
    };

    dd.compareSchemas(currentSchema, newSchema);
    result.UP = dd.commands('drop');
    dd.compareSchemas(newSchema, currentSchema);
    result.DOWN = dd.commands('drop');

    var name = moment().format('DD-MM-YYYY_hh:mm:ss');
    fs.mkdirSync(__dirname + '/../dbdata/migrations/' + name);
    fs.writeFileSync(__dirname + '/../dbdata/migrations/' + name + '/up.sql', result.UP);
    fs.writeFileSync(__dirname + '/../dbdata/migrations/' + name + '/down.sql', result.DOWN);

    console.log('Migration files created');

    newSchema = JSON.stringify(newSchema);

    fs.writeFile(__dirname + '/../dbdata/currentState.json', newSchema, (err) => {

        if (err) {
            console.log(err);
            process.exit(1);
        }

        console.log('Current state of db schema updated');
        process.exit(0);
    });
});
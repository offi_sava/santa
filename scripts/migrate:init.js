'use strict';

process.env['SERVER_CONFIG'] = __dirname + '/../locals/config.json';

const dbdiff = require('dbdiff'),
    config = require('../config/default'),
    fs = require('fs');

dbdiff.describeDatabase(config.database).then((schema) => {

    let data = JSON.stringify(schema);

    fs.writeFile(__dirname +'/../dbdata/currentState.json', data, (err) => {
        if (err) {
            console.log(err);
        }

        console.log('Current state of db schema updated');
        process.exit(0);
    });


}).catch((err) => {
    console.log(err);
    process.exit(1);
});
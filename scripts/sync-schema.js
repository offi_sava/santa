'use strict';

const promisify = require('util').promisify,
      Injector = require('../lib/init'),
      fs = require('fs'),
      _ = require('lodash'),
      Autowired = require('autowired-js');


const dirDb = [require('../services/db')];
const dirInitial = __dirname + '/../dbdata/initial-data.sql';
const dirCountries = __dirname + '/../dbdata/countries_data.sql';

let inj = new Injector();
let readFile = promisify(fs.readFile);


(async function() {
   let app =  await inj.register(dirDb);
   let db = app.getBean('db');


    await db.orm.sync({force: true});
    console.log('All tables created');

    let data = await readFile(dirInitial, 'utf8');
    await db.orm.query(data);
    console.log('Services inserted');

    data = await readFile(dirCountries, 'utf8');
    await db.orm.query(data);
    console.log('Countries inserted');

    process.exit(0);

})().catch(e => {
    console.log(e.message);
    process.exit(1);

});
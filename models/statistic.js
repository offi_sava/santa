module.exports = (orm, dataTypes) => {
    const Statistic = orm.define("statistic", {
        id: {
            type: dataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
            field: 'id'
        },
        service: {
            type: dataTypes.STRING,
            field: 'service'
        },
        value: {
            type: dataTypes.STRING,
            field: 'value'
        }
    }, {
        timestamps: false
    });


    return Statistic;
};
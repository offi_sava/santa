module.exports = (orm, dataTypes) => {
    const UserRoom = orm.define("users_room", {
        userId: {
            type: dataTypes.BIGINT,
            field: 'user_id',
            allowNull: false,
            primaryKey: true
        },
        roomId: {
            type: dataTypes.BIGINT,
            field: 'room_id',
            allowNull: false,
        },
        active: {
            type: dataTypes.TINYINT,
            field: 'active',
            defaultValue: '1'
        }
    }, {
        timestamps: false
    });

    UserRoom.associate = function (models) {
        UserRoom.belongsTo(models.User, {as: 'user'});
        UserRoom.belongsTo(models.Room, {as: 'room'});
    };
    return UserRoom;
};
module.exports = (orm, dataTypes) => {
    const Call = orm.define("call", {
        id: {
            type: dataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
            field: 'id'
        },
        wish: {
            type: dataTypes.STRING,
            field: 'wishes'
        },
        phone: {
            type: dataTypes.STRING,
            field: 'phone'
        },
        createdAt: {
            type: dataTypes.DATE,
            allowNull: false,
            field: 'created_at',
            defaultValue: orm.fn('NOW')
        },
        updatedAt: {
            type: dataTypes.DATE,
            allowNull: false,
            field: 'updated_at',
            defaultValue: orm.fn('NOW')
        }
    });

    return Call;
};
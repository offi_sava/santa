module.exports = (orm, dataTypes) => {
    const UserSanta = orm.define("users_santas", {
        userId: {
            type: dataTypes.BIGINT,
            field: 'user_id',
            allowNull: false,
            primaryKey: true
        },
        santaForUserId: {
            type: dataTypes.BIGINT,
            field: 'santa_for_user_id',
            allowNull: false,
        },
        status: {
            type: dataTypes.ENUM('delivered','none','sent'),
            field: 'status',
        },
        createdAt: {
            type: dataTypes.DATE,
            allowNull: false,
            field: 'created_at',
            defaultValue: orm.fn('NOW')
        },
        updatedAt: {
            type: dataTypes.DATE,
            allowNull: false,
            field: 'updated_at',
            defaultValue: orm.fn('NOW')
        }
    });

    UserSanta.associate = function (models) {
        UserSanta.belongsTo(models.User, {as: 'user'});
       // UserSanta.belongsTo(models.User, {as: 'userSanta', foreign_key: 'santaForUserId'});
    };
    return UserSanta;
};
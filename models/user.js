module.exports = (orm, dataTypes) => {
    const User = orm.define("user", {
        id: {
            type: dataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
            field: 'id'
        },
        telegramId: {
            type: dataTypes.BIGINT,
            field: 'telegram_id'
        },
        telegramLogin: {
            type: dataTypes.STRING,
            field: 'telegram_login'
        },
        dateLast: {
            type: dataTypes.DATE,
            field: 'date_last',
            defaultValue: orm.fn('NOW')
        },
        country: {
            type: dataTypes.ENUM('UA','RU','BY','KZ','MD','AZ','KR','AR','GE','IS','UZ'),
            field: 'country',
        },
        city: {
            type: dataTypes.STRING,
            field: 'city'
        },
        phone: {
            type: dataTypes.STRING,
            field: 'phone'
        },
        address: {
            type: dataTypes.STRING,
            field: 'address'
        },
        fullname: {
            type: dataTypes.STRING,
            field: 'fullname'
        },
        availableCalls: {
            type: dataTypes.TINYINT.UNSIGNED,
            field: 'available_calls',
            defaultValue: '0'
        },
        wish: {
            type: dataTypes.TEXT,
            field: 'wish'
        }

    }, {
        timestamps: false
    });

    User.associate = function (models) {
         User.hasMany(models.UserSanta, {as: 'userSanta', foreignKey: 'userId'});
         User.hasMany(models.UserSanta, {as: 'santaForUsers', foreignKey: 'santaForUserId'});
         User.hasMany(models.UserRoom, {as: 'room', foreignKey: 'userId'});
         User.hasMany(models.Order, {as: 'orders', foreignKey: 'userId'});

      };

    return User;
};
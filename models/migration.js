module.exports = (orm, dataTypes) => {
    const Migration = orm.define("migration", {
        id: {
            type: dataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
            field: 'id'
        },
        migration: {
            type: dataTypes.STRING,
            field: 'migration'
        },
        batch: {
            type: dataTypes.INTEGER,
            field: 'batch'
        },
    }, {
        timestamps: false
    });

    return Migration;
};
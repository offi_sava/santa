module.exports = (orm, dataTypes) => {
    const Room = orm.define("room", {
        userId: {
            type: dataTypes.BIGINT,
            field: 'user_id',
            allowNull: false,
        },
        roomId: {
            type: dataTypes.BIGINT,
            field: 'room_id',
            allowNull: false,
        },
        started: {
            type: dataTypes.STRING,
            field: 'started'
        },
        type: {
            type: dataTypes.ENUM('yes','no'),
            field: 'status',
            defaultValue: 'no'
        },
        sendMail: {
            type: dataTypes.ENUM('yes','no'),
            field: 'send_mail',
            defaultValue: 'no'
        },
        createdAt: {
            type: dataTypes.DATE,
            allowNull: false,
            field: 'created_at',
            defaultValue: orm.fn('NOW')
        },
        updatedAt: {
            type: dataTypes.DATE,
            allowNull: false,
            field: 'updated_at',
            defaultValue: orm.fn('NOW')
        }
    });

    Room.associate = function (models) {
          Room.hasMany(models.UserRoom, {as: 'userRooms', foreignKey: 'roomId'});
    };
    return Room;
};
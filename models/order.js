module.exports = (orm, dataTypes) => {
    const Order = orm.define("order", {
        id: {
            type: dataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
            field: 'id'
        },
        userId: {
            type: dataTypes.BIGINT,
            field: 'user_id',
            allowNull: false,
        },
        phone: {
            type: dataTypes.STRING,
            field: 'phone'
        },
        audioUrl: {
            type: dataTypes.STRING,
            field: 'audio_url'
        },
        status: {
            type: dataTypes.ENUM('created','pending','called','finished','aborted'),
            field: 'status',
            defaultValue: 'no'
        },
        createdAt: {
            type: dataTypes.DATE,
            allowNull: false,
            field: 'created_at',
            defaultValue: orm.fn('NOW')
        },
        updatedAt: {
            type: dataTypes.DATE,
            allowNull: false,
            field: 'updated_at',
            defaultValue: orm.fn('NOW')
        }
    });

    Order.associate = function (models) {
        Order.belongsTo(models.User, {as: 'user'});
        // UserSanta.belongsTo(models.User, {as: 'userSanta', foreign_key: 'santaForUserId'});
    };
    return Order;
};